package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests

    @InjectMocks
    private HolyGrail holyGrail;

    @Test
    public void whenGetHolyWishAfterMakeAWishItShouldReturnTheWish(){
        String wish = "Kaya";
        holyGrail.makeAWish(wish);
        HolyWish holyWish = holyGrail.getHolyWish();
        String wished = holyWish.getWish();
        assertEquals(wish,wished);
    }
}
